import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import ProjectCard from "./ProjectCards";
import Particle from "../Particle";
import leaf from "../../Assets/Projects/leaf.png";
import emotion from "../../Assets/Projects/emotion.png";
import editor from "../../Assets/Projects/codeEditor.png";
import chatify from "../../Assets/Projects/chatify.png";
import suicide from "../../Assets/Projects/suicide.png";
import bitsOfCode from "../../Assets/Projects/blog.png";

function Projects() {
  return (
    <Container fluid className="project-section">
      <Particle />
      <Container>
        <h1 className="project-heading">
          My Recent <strong className="purple">Works </strong>
        </h1>
        <p style={{ color: "white" }}>
          Here are a few projects I've worked on recently.
        </p>
        <Row style={{ justifyContent: "center", paddingBottom: "10px" }}>
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={chatify}
              isBlog={false}
              title="INoteBook"
              description="This is a Web Application which solves the problem of storing notes without worrying to loose it. Front-end is build using ReactJs and backend on Express. MongoDb Atlas database is used to store the user credentials and personal notes."
              ghLink="https://github.com/1711naveen/iNotebook-Frontend"
            />
          </Col>
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={bitsOfCode}
              isBlog={false}
              title="Sorting-Visualiser"
              description="Used the knowledge of Data Sturcture and Algorithms to implement sorting of array of numbersand visualising it with the help of HTML, CSS and Javascript."
              ghLink="https://github.com/1711naveen/Sorting-Visualiser"
            />
          </Col>
        </Row>
      </Container>
    </Container>
  );
}

export default Projects;

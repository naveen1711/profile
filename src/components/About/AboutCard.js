import React from "react";
import Card from "react-bootstrap/Card";
import { ImPointRight } from "react-icons/im";

function AboutCard() {
  return (
    <Card className="quote-card-view">
      <Card.Body>
        <blockquote className="blockquote mb-0">
          <p style={{ textAlign: "justify" }}>
            Hi Everyone, I am <span className="purple">Naveen Yadav </span>
            from <span className="purple"> Kanpur, UttarPradesh, India.</span>
            <br />
            I have completed
            <span className="purple"> B.Tech&nbsp;</span>
            in
            <span className="purple"> Mechanical Engineering&nbsp;</span>
            from
            <span className="purple"> Madan Mohan Malaviya University of Technology, Gorakhpur.&nbsp;</span>
            Interested in
            <span className="purple"> Machine Learning & Artificial Intelligence.</span>
            <br />
            <br />
            <br />
            Apart from coding, some other activities that I love to do!
          </p>
          <ul>
            <li className="about-activity">
              <ImPointRight /> Playing Chess
            </li>
            <li className="about-activity">
              <ImPointRight /> Reading Books
            </li>
            <li className="about-activity">
              <ImPointRight /> Watching Anime
            </li>
          </ul>
        </blockquote>
      </Card.Body>
    </Card>
  );
}

export default AboutCard;
